PROGRAM shallow_water

! Use a leap-frog method to solve the linearized shallow-water
! equations 

IMPLICIT NONE 

INTEGER, PARAMETER :: m=200  ! there will be mXm cells in the domain 
DOUBLE PRECISION :: ttot=10.d0  ! solve from time=0 to time=ttot
DOUBLE PRECISION :: c=1.d0 ! wave speed=sqrt(g*h_0)  
DOUBLE PRECISION :: cfl=.5d0 ! c*dt/dx - cannot be too large
DOUBLE PRECISION :: kx=1.d0  ! Wave number in x direction
DOUBLE PRECISION :: ky=2.d0  ! Wave number in y direction 
LOGICAL :: exsol=.TRUE. ! We have an exact solution to check accuracy 
INTEGER :: jout=10 ! Output error or solution size every jout steps 
INTEGER :: jwrite=1 ! Number of output files for plotting 
DOUBLE PRECISION, DIMENSION(0:m+1,0:m+1) :: h,u,v
DOUBLE PRECISION, DIMENSION(m,m) :: hold,uold,vold 
DOUBLE PRECISION, DIMENSION(m) :: x,y
DOUBLE PRECISION :: dx,dt,t,err,size,lam,lamcs,hloc,uloc,vloc,swap 
INTEGER :: j,k,nsteps,it,jdat
CHARACTER*19, DIMENSION(1) :: write_files=(/ "swsoldata_case1.01" /)

! We will call a subroutine to compute the necessary initial data 
! or the exact solution if available
!
INTERFACE 
  SUBROUTINE ShallowWaterSol(hloc,uloc,vloc,xloc,yloc,t,kx,ky,c)
    DOUBLE PRECISION, INTENT(IN) :: xloc,yloc,kx,ky,c,t
    DOUBLE PRECISION, INTENT(OUT) :: hloc,uloc,vloc 
  END SUBROUTINE ShallowWaterSol
END INTERFACE 

! Set up the mesh and time step 

dx=1.d0/DBLE(m) 
dt=cfl*dx/c 
nsteps=CEILING(ttot/dt)
dt=ttot/DBLE(nsteps)
lam=dt/dx 
lamcs=lam*c*c

DO j=1,m
  x(j)=DBLE(j)*dx 
END DO

y=x

! Save basic input description 

OPEN(UNIT=40,FILE="case1.txt",STATUS="UNKNOWN")
WRITE(40,*)"m=",m
WRITE(40,*)"nsteps=",nsteps
WRITE(40,*)"ttot=",ttot
WRITE(40,*)"c=",c
WRITE(40,*)"kx=",kx
WRITE(40,*)"ky=",ky
CLOSE(UNIT=40)

! Save x and y in a file for plotting

OPEN(UNIT=20,FILE="x.txt",STATUS="UNKNOWN")
OPEN(UNIT=21,FILE="y.txt",STATUS="UNKNOWN")

DO j=1,m
  WRITE(20,*)x(j)
END DO
DO k=1,m
  WRITE(21,*)y(k)
END DO

CLOSE(UNIT=20)
CLOSE(UNIT=21)

! Open output file

OPEN(UNIT=29,FILE="sw_error_data.case1",STATUS="UNKNOWN")

! Initial data - note we need it at 2 time levels to get going
! but we assume that is taken care of in the ShallowWaterSol routine 

DO k=1,m
  DO j=1,m
    CALL ShallowWaterSol(h(j,k),u(j,k),v(j,k),x(j),y(k),0.d0,kx,ky,c)
  END DO
END DO 

CALL PeriodicExtension ! extends h,u,v by periodicity 

DO k=1,m
  DO j=1,m
    CALL ShallowWaterSol(hold(j,k),uold(j,k),vold(j,k),x(j),y(k),-dt,kx,ky,c)
  END DO
END DO 

! Now march 

DO it=1,nsteps 
 DO k=1,m
   DO j=1,m
     hold(j,k)=hold(j,k)-lam*(u(j+1,k)-u(j-1,k)+v(j,k+1)-v(j,k-1)) 
     uold(j,k)=uold(j,k)-lamcs*(h(j+1,k)-h(j-1,k)) 
     vold(j,k)=vold(j,k)-lamcs*(h(j,k+1)-h(j,k-1)) 
   END DO
 END DO
!
! Swap and extend
!
 DO k=1,m
   DO j=1,m
     swap=hold(j,k)
     hold(j,k)=h(j,k)
     h(j,k)=swap
     swap=uold(j,k)
     uold(j,k)=u(j,k)
     u(j,k)=swap
     swap=vold(j,k)
     vold(j,k)=v(j,k)
     v(j,k)=swap
   END DO
 END DO
 CALL PeriodicExtension 
!
! Check for output and saving
!
 IF (MOD(it,jout)==0) THEN
   t=DBLE(it)*dt
   IF (exsol) THEN
     err=0.d0
     size=0.d0
     DO k=1,m
       DO j=1,m
         CALL ShallowWaterSol(hloc,uloc,vloc,x(j),y(k),t,kx,ky,c)
         err=err+dx*dx*((c*(h(j,k)-hloc))**2+(u(j,k)-uloc)**2+(v(j,k)-vloc)**2)
         size=size+dx*dx*((c*hloc)**2+uloc**2+vloc**2)
       END DO
     END DO 
     WRITE(29,*)t,SQRT(err/size)  ! Relative error 
   ELSE 
     size=0.d0 
     DO k=1,m
       DO j=1,m
         size=size+dx*dx*((c*h(j,k))**2+u(j,k)**2+v(j,k)**2)
       END DO
     END DO 
     WRITE(29,*)t,SQRT(size)  
   END IF
 END IF
!
 IF (MOD(jwrite*it,nsteps)==0) THEN
   jdat=jwrite*it/nsteps
   OPEN(UNIT=30,FILE=write_files(jdat),STATUS="UNKNOWN")
   DO k=1,m
     DO j=1,m
       WRITE(30,*)h(j,k),u(j,k),v(j,k)
     END DO
   END DO
   CLOSE(UNIT=30)
 END IF 
!
END DO 
  
CLOSE(UNIT=29) 

! Done 

CONTAINS 

  SUBROUTINE PeriodicExtension 
!
! An internal subroutine knows the local variables
!
    h(0,1:m)=h(m,1:m)
    u(0,1:m)=u(m,1:m)
    v(0,1:m)=v(m,1:m)
    h(m+1,1:m)=h(1,1:m)
    u(m+1,1:m)=u(1,1:m)
    v(m+1,1:m)=v(1,1:m)
    h(0:m+1,0)=h(0:m+1,m)
    u(0:m+1,0)=u(0:m+1,m)
    v(0:m+1,0)=v(0:m+1,m)
    h(0:m+1,m+1)=h(0:m+1,1)
    u(0:m+1,m+1)=u(0:m+1,1)
    v(0:m+1,m+1)=v(0:m+1,1)
!
  END SUBROUTINE PeriodicExtension
!

END PROGRAM shallow_water 
